# whiletruedoio.general.cockpit_podman

A Podman server for container workloads, featuring the Cockpit Web interface,
which can also be used to create and manage container workloads.

## Description

[Cockpit](https://cockpit-project.org/) is a web interface for Linux management.
It can be used to review logs, update a machine or configure the network and
much more. With this playbook, you will get a minimal container host. The
package of Podman and Cockpit, provides a convenient and easy way to manage
container workloads on a single machine.

It is mostly intended for private networks, like in companies or at home.

## Dependencies

None.

## Conflicts

None.

## Variables

TBD

## Used roles

TBD

## Additional hints

TBD

## Example

TBD
