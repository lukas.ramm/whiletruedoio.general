# whiletruedoio.general.cockpit_home

A cockpit based home server, which provides useful tooling for home networks.

## Description

The cockpit_home playbook is fitted for becoming your small home server. Extend
it with kvm or podman and you should have a sufficient machine for all kinds of
home workload. Install NFS and you should be ready for a storage node. The web
ui makes it easy to administrate and MDNS helps with discovering the machine in
small networks. The firewall has more relaxed settings than usual.

## Dependencies

None.

## Conflicts

None.

## Variables

TBD

## Used roles

TBD

## Additional hints

TBD

## Example

TBD
