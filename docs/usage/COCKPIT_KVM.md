# whiletruedoio.general.cockpit_kvm

A KVM Hypervisor, including the Cockpit Web interface, which can also be used
to create and manage virtual machines.

## Description

[Cockpit](https://cockpit-project.org/) is a web interface for Linux management.
It can be used to review logs, update a machine or configure the network and
much more. With this playbook, you will get a minimal hypervisor for KVM
virtualization. As a package of KVM and Cockpit, you will have a small, but
convenient way to manage virtual machines in a graphical way.

It is mostly intended for private networks, like in companies or at home.

## Dependencies

None.

## Conflicts

None.

## Variables

TBD

## Used roles

TBD

## Additional hints

TBD

## Example

TBD
