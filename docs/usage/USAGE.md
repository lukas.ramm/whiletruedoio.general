---
tags:
  - "usage"
  - "playbooks"
---

# Usage

This document describes how you can use the collection and it's included
content.

## Requirements

Before you can use the collection, you need to ensure that the below
requirements are met. The collection may work in settings other than the below,
but I am unable to support you properly in most cases.

### Control Node

You will need Ansible in a supported version on the control node (the node
executing playbooks). Installing Ansible on macOS and Linux can be done in a
Python Virtualenv (venv) as shown below.

```shell
# Create a python virtual environment
$ python -m venv .venv

# Activate the environment
$ source .venv/bin/activate

# Install Ansible Core
$ pip install ansible-core>=2.10

# Check Ansible version
$ ansible --version
```

The
[Ansible install guide](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
offers more options to install Ansible.

### Managed Node

Supporting all operating systems as the target (managed node), is out of scope
for small projects like this one. Therefore, I had to limit the support
according to the below list.

- [CentOS Stream 9](https://www.centos.org/)
- [AlmaLinux OS 9.x](https://almalinux.org/)
- [Rocky Linux 9.x](https://rockylinux.org/)
- [Red Hat Enterprise Linux 9.x](https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux)

In case you are seeking support for other Linux operating systems, I would be
happy to discuss this with you and I am eager to help you onboarding with the
[development](../development/REQUIREMENTS.md).

## Installation

Before using the collection, you need to install it to your control node.

### galaxy.ansible.com

The preferred installation method is from Ansible Galaxy. This can be done via:

```shell
ansible-galaxy collection install whiletruedoio.general
```

Or include it in your own `requirements.yml` file and install it from there:

```yaml
---
collections:
  - name: "whiletruedoio.general"
```

```shell
ansible-galaxy collection install -r requirements.yml
```

### Release Package

Alternatively, you can download the package from the
[Releases page](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/releases)
and install it to your machine.

```shell
# Install
ansible-galaxy collection install path/to/package.tar.gz
```

## Running

After meeting the requirements and installing the collection, you are ready to
run the included content.

### Using a playbook

Using one of the included playbooks is pretty easy.

```shell
ansible-playbook -i INVENTORY -e target="GROUPorHOST" whiletruedoio.general.PLAYBOOK
```

### Playbook Naming

The naming of the playbooks follows the following schema:

```txt
<SCOPE>_<PURPOSE>_[<ENV>]_[<SECURITY>].ks.cfg
```

Whereas:

**SCOPE** defines the general **idea** of the resulting installation.

- minimal: a minimal installation for general purpose and customization
- cockpit: a variation with the cockpit web interface for management
- graphical: a variant with a desktop frontend

**PURPOSE** defines some use case variants that show **how** the resulting
installation can be used.

- base: provides only the base
- podman: providing the Podman container engine
- kvm: providing the KVM hypervisor
- home: a home server setup

**ENV** is optional and defines the environment **where** the image should be
used.

- phy: a physical or bare metal installation
- vir: a virtual machine on a hypervisor, including required agent packages
- cld: a machine that is intended to be running on one of the many public cloud
  providers like Amazon AWS, Azure, Exoscale, etc.

**SECURITY** is optional and defines an additional security scope, according to
[OpenSCAP](https://www.open-scap.org/).

- cisl1: [CIS Benchmark for Level 1 - Server](https://static.open-scap.org/ssg-guides/ssg-rhel9-guide-cis_server_l1.html)
- cisl2: [CIS Benchmark for Level 2 - Server](https://static.open-scap.org/ssg-guides/ssg-rhel9-guide-cis.html)
- ospp: [Protection profile for General Purpose Operating Systems](https://static.open-scap.org/ssg-guides/ssg-rhel9-guide-ospp.html)

### Included Playbooks

The following playbooks are included.

#### Minimal Setups

Minimal setups are meant to provide the bare minimum to run the desired
application. They do feature some security enhancements, though.

- [whiletruedoio.general.minimal_base](./MINIMAL_BASE.md)
- [whiletruedoio.general.minimal_podman](./MINIMAL_PODMAN.md)
- [whiletruedoio.general.minimal_kvm](./MINIMAL_KVM.md)
- [whiletruedoio.general.minimal_gitlab_runner](./MINIMAL_GITLAB_RUNNER.md)

#### Cockpit Setups

Cockpit setups feature the Cockpit Web interface for management and might be
a good idea, if you want to manage machines w/o the terminal or automation.

- [whiletruedoio.general.cockpit_base](./COCKPIT_BASE.md)
- [whiletruedoio.general.cockpit_home](./COCKPIT_HOME.md)
- [whiletruedoio.general.cockpit_podman](./COCKPIT_PODMAN.md)
- [whiletruedoio.general.cockpit_kvm](./COCKPIT_KVM.md)

#### Maintenance

In addition, there are a couple of maintenance playbooks.

- [whiletruedoio.general.update_machine](./UPDATE_MACHINE.md)

### Using a role

The general usage of included roles is intended to be as easy as possible.
Therefore, each role provides some sane defaults. In case the role requires
a password or other mandatory input, it will fail gracefully.

Furthermore, the roles don't need to be executed with `become` on the playbook
level. Instead, this is done per task, and only if needed.

Most roles don't demand such things and can be used like so:

```yaml
- name: "your playbook"
  hosts: "yourhosts"

  tasks:

    - name: "Use bash Role"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.bash"
```

The classic way is also supported.

```yaml
- name: "your playbook"
  hosts: "yourhosts"

  roles:

    - name: "whiletruedoio.general.bash"
```

### Included roles

The following roles are included in the collection. Each of the provides its own
documentation for more details.

- [whiletruedoio.general.ansible](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/ansible)
- [whiletruedoio.general.audit](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/audit)
- [whiletruedoio.general.avahi](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/avahi)
- [whiletruedoio.general.bash](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/bash)
- [whiletruedoio.general.certbot](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/certbot)
- [whiletruedoio.general.chrony](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/chrony)
- [whiletruedoio.general.cockpit](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/cockpit)
- [whiletruedoio.general.dnf](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/dnf)
- [whiletruedoio.general.epel](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/epel)
- [whiletruedoio.general.fail2ban](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/fail2ban)
- [whiletruedoio.general.firewalld](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/firewalld)
- [whiletruedoio.general.git](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/git)
- [whiletruedoio.general.gitlab_runner](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/gitlab_runner)
- [whiletruedoio.general.hostname](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/hostname)
- [whiletruedoio.general.k3s](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/k3s)
- [whiletruedoio.general.kdump](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/kdump)
- [whiletruedoio.general.kvm](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/kvm)
- [whiletruedoio.general.openscap_scanner](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/openscap_scanner)
- [whiletruedoio.general.pcp](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/pcp)
- [whiletruedoio.general.podman](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/podman)
- [whiletruedoio.general.selinux](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/selinux)
- [whiletruedoio.general.sshd](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/sshd)
- [whiletruedoio.general.sudo](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/sudo)
- [whiletruedoio.general.suspend](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/suspend)
- [whiletruedoio.general.thermald](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/thermald)
- [whiletruedoio.general.timesyncd](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/timesyncd)
- [whiletruedoio.general.timezone](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/timezone)
- [whiletruedoio.general.tmux](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/tmux)
- [whiletruedoio.general.tuned](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/tuned)
- [whiletruedoio.general.update](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/update)
- [whiletruedoio.general.usbguard](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/usbguard)
- [whiletruedoio.general.user](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/user)
- [whiletruedoio.general.vm_guest](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/tree/main/roles/vm_guest)
