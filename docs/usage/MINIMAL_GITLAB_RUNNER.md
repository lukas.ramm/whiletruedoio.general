# whiletruedoio.general.minimal_gitlab_runner

A minimal GitLab Runner, using Podman as executor.

## Description

The Podman + GitLab Runner setups allows to create and register runners for
your GitLab instance or GitLab SaaS. It also enables some additional garbage
collection timers for Podman.

## Dependencies

None.

## Conflicts

None.

## Variables

TBD

## Used roles

TBD

## Additional hints

TBD

## Example

TBD
