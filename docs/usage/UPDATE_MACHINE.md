# whiletruedoio.general.update_machine

A playbook intended to update a system and its installed software.

## Description

The playbook takes care of your update demands. For now, it supports only the
system packages, but will be extended with other package formats like Flatpak,
SNAP, etc. as soon as the roles are done.

## Dependencies

None.

## Conflicts

None.

## Variables

TBD

## Used roles

TBD

## Additional hints

TBD

## Example

TBD
