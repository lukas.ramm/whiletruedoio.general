---
tags:
  - "development"
---

# Testing

It is common practice to test your code before opening a merge request. There
will be automated testing via CI/CD, but you can also test a lot, locally.

## Command Line

If you already set up the above Environment, you will have access to the
following command line tools.

```shell
# Ansible Lint
$ ansible-lint .

# Ansible Syntax Check
$ ansible-playbook --syntax-check path/to/playbook.yml

# yamllint
$ yamllint .
```

In case you are using [Steampunk Spotter](https://steampunk.si/spotter/), you
can also run the spotter tests.

```shell
# Run spotter
$ spotter --api-token YOURTOKEN scan .
```

## Pipeline

For automated testing and quality assurance, I use
[GitLab-CI](https://docs.gitlab.com/ee/ci/). You can read the pipeline in the
`.gitlab-ci.yml` file. In general the stages for a merge process are:

- Lint (linting and syntax checks)
- Build (building interims artifacts)
- Test (testing interims artifacts functionally)

You can download the artifacts in your pipeline (or other pipelines), if you
desire.
