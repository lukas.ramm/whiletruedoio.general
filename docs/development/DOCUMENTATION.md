---
tags:
  - "development"
---

# Documentation

The documentation for this repository is done in
[Markdown](https://commonmark.org/). The desired conventions are tested with
[Markdownlint](https://github.com/DavidAnson/markdownlint). Furthermore, the
files in the `docs/` directory will be compiled to a website using
[mkdocs](https://www.mkdocs.org/).

You can run this compilation on your local machine and test it with some simple
commands. Beforehand, you should ensure to have a Python development environment
as explained above.

```shell
# markdownlint
$ markdownlint *.md
$ markdownlint docs/*md

# Start a local server
$ mkdocs serve -s

# Do the build process locally
$ mkdocs build -s
```
