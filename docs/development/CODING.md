---
tags:
  - "development"
---

# Coding

When it comes to coding, the procedure is pretty simple, but you should take
care of some conventions.

## Workflow

The general process of contributing is pretty simple:

1. Open an issue, describing the feature/fix you want to implement

OR

1. Remark in an existing issue, that you want to take care of the same
2. Fork the repository
3. Implement your work
4. Update the documentation and tests (see below)
5. Open a merge request/pull request using
   [conventional commits](https://www.conventionalcommits.org/en/)
6. Check if the pipeline succeeds and implement changes if needed
7. Wait for feedback from the repository maintainers

In case of any questions in the development process, feel free to ping me via
the issue.

## Conventions

I try to make all conventions "obvious". This means, that there will be a
linting file for things or a test in a pipeline. If you stick to the
[editor and environment](./REQUIREMENTS.md), you should be good to go.
