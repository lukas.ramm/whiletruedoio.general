---
tags:
  - "development"
---

# Requirements

For development, testing and debugging, you will need a working development
environment. The setup for the same is pretty simple.

## Code

The first thing you need is the code from the repository. If you want to
contribute, you should make a fork and change the url accordingly.

```shell
# Clone the repository
$ git clone https://gitlab.com/whiletruedoio/whiletruedoio.general.git

# Change into the directory
$ cd infrastructure/
```

## Python Virtual Environment

After cloning the repository and changing into it, you should set up a virtual
environment for Python development. Most IDEs and Editors like VSCode will
detect this automatically and use it. You will need this for Ansible,
Documentation and some test tools.

```shell
# Create a python virtual environment
$ python -m venv .venv

# Activate the environment
$ source .venv/bin/activate

# Install the required packages
$ pip install -r requirements.txt
```

## Editor

Lastly, you might need an editor, that supports
[Editorconfig](https://editorconfig.org/). This will ensure that files are
properly formatted and some style guidelines are applied properly. I am using
[VSCode](https://code.visualstudio.com/).
