![Pipeline](https://gitlab.com/whiletruedoio/whiletruedoio.general/badges/main/pipeline.svg)
![Issues](https://img.shields.io/gitlab/issues/all/whiletruedoio%2Fwhiletruedoio.general)
![Release](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/badges/release.svg)
![License](https://img.shields.io/gitlab/license/whiletruedoio%2Fwhiletruedoio.general)

# whiletruedoio.general

An Ansible collection for general purpose roles, plugins and playbooks.

## Disclaimer

I am currently migrating a lot of stuff over to GitLab. Therefore, this
collection should be considered broken or at least, under heavy development.

There are some ToDo items, that need to be tackled. You can find them in the
[TODO.md](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/blob/main/docs/TODO.md).

## Motivation

At [while-true-do.io](https://while-true-do.io), I wanted to automate as much as
possible and make this approach transparent. I am using Ansible for many things,
and thought it might be a good idea to make the collections publicly available.

## Description

The collection includes roles and plugins for general purpose tasks, like
setting a hostname, configuring some services or performing updates.

Additionally, you will find ready-to-use playbooks available that can configure
a minimal server, container environment or virtualization machine properly.

Lastly, I put a lot of focus on best practices and provide sane defaults for
security and re-usability.

## Quick Start

Install the collection:

```shell
# Install collection
$ ansible-galaxy collection install whiletruedoio.general
```

Run a playbook:

```shell
# Run playbook
$ ansible-playbook -i <INVENTORY> -e target=<TARGET> whiletruedoio.general.minimal_base_server
```

Or use a role in your playbooks:

```yaml
- hosts: "all"

  tasks:

    - name: "Import podman Role"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.podman"
```

## Usage

The usage of this repository and its code is explained in the
[Usage documentation](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/blob/main/docs/USAGE.md).

## Contribute

Thank you so much for considering to contribute. Please don't hesitate to
provide feedback, report bugs, request features or contribute code. Please
make yourself comfortable with the
[contributing guidelines](https://gitlab.com/whiletruedoio/gitlab-profile/-/blob/main/docs/CONTRIBUTING.md)
first.

### Issues

Opening issues and reporting bugs is pretty easy. Please feel free to open a
report via the issue tracker or send an e-mail.

- [Issue Tracker](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/issues)
- [Support Mail](mailto:support@while-true-do.io)

### Development

In case you consider to contribute with your development, please check out the
[development documentation](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/blob/main/docs/development/REQUIREMENTS.md).

## License

Except otherwise noted, all work is
[licensed](https://gitlab.com/whiletruedoio/whiletruedoio.general/-/blob/main/LICENSE)
under a [BSD-3-Clause License](https://opensource.org/licenses/BSD-3-Clause).

## Contact

In case you want to get in touch with me or reach out for general questions,
please use one of the below contact details

- Blog: [blog.while-true-do.io](https://blog.while-true-do.io)
- Code: [gitlab.com/whiletruedoio](https://gitlab.com/whiletruedoio)
- Chat: [#whiletruedoio-community](https://matrix.to/#/#whiletruedoio-community:matrix.org)
- Mail: [hello@while-true-do.io](mailto:hello@while-true-do.io)
