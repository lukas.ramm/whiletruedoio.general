# whiletruedoio.general.suspend

An Ansible Role to configure the suspend, idle, sleep or hibernate behaviour
of a system.

## Description

In some scenarios, you might want to force the system avoiding suspend or
hibernate. This role is meant to support with the configuration of the same.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.suspend"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.suspend"
      vars:
        suspend_sleep_target_masked: true
```
