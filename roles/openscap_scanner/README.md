# whiletruedoio.general.openscap_scanner

An Ansible Role to install and configure the openSCAP scanner.

## Description

OpenSCAP is s a suite of tools to audit, configure and mitigate security
policies. It is widely used in RedHat family systems. The scanner is meant to
audit a single machine and creating reports.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.openscap_scanner"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.openscap_scanner"
      vars:
        openscap_scanner_package_state: "latest"
```
