# whiletruedoio.general.update

An Ansible Role to install system updates.

## Description

This role does system updates. This is mostly meant for maintenance purposes,
but can also be useful for initial deployments.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.update"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.update"
      vars:
        update_reboot_timeout: "120"
```
