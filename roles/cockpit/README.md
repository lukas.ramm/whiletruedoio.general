# whiletruedoio.general.cockpit

An Ansible Role to install and configure cockpit.

## Description

The role installs the Cockpit Web interface on a machine, starts the services
and configures the firewall, if present. Additionally, it detects the existence
of some other software like Podman or KVM and installs the fitting Cockpit
applications for the same.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.cockpit"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.cockpit"
      vars:
        cockpit_package_state: "latest"
```
