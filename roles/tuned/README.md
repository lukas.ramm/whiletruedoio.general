# whiletruedoio.general.tuned

An Ansible Role to install and configure the TuneD service.

## Description

TuneD is power- and performance profile daemon, that configures kernel
parameters or system control parameters to ensure a machine is configured for
the proper purpose. This role installs and configures TuneD and automatically
chooses a profile based on the discovered Linux OS environment.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.tuned"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.tuned"
      vars:
        tuned_package_state: "latest"
```
