# whiletruedoio.general.kdump

An Ansible Role to install and configure kdump.

## Description

Kdump provides deep insights into kernel crashes and driver issues. The role
installs and configures kdump, so you can make use of it afterwards.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.kdump"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.kdump"
      vars:
        kdump_package_state: "latest"
```
