# whiletruedoio.general.kvm

An Ansible Role to install a KVM hypervisor.

## Description

The role installs and configures KVM, libvirt and CLI client tools for
virtualization. Additionally, Kernel Same Page merging and nested virtualization
can be configured.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.kvm"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.kvm"
      vars:
        kvm_package_state: "latest"
```
