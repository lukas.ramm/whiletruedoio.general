# whiletruedoio.general.fail2ban

An Ansible Role to install and configure fail2ban.

## Description

This role installs fail2ban from the EPEL repositories and ensures that SSH is
protected by it.

## Dependencies

The role will automatically execute the following roles beforehand:

- [whiletruedoio.general.epel](./EPEL.md)

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.fail2ban"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.fail2ban"
      vars:
        fail2ban_package_state: "latest"
```
