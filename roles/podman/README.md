# whiletruedoio.general.podman

An Ansible Role to install and configure podman.

## Description

The role installs, configures and enables Podman on a given machine. It does
take care of some additional services for automatic restarts or updates, too.
Furthermore, there is an optional scheduler to prune dangling images, containers
or the system.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.podman"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.podman"
      vars:
        podman_package_state: "latest"
```
