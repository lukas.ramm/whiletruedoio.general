# whiletruedoio.general.selinux

An Ansible Role to install and configure selinux.

## Description

The role ensures that SELinux is in the desired state and troubleshooting
packages are present alongside.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.selinux"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.selinux"
      vars:
        selinux_package_state: "latest"
```
