# whiletruedoio.general.pcp

An Ansible Role to install and configure Performance Co-Pilot.

## Description

The role takes care of installing and configuring a Performance Co-Pilot client
on a given machine. This can be useful for persisting performance data in
Cockpit or to export it for other monitoring tools.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.pcp"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.pcp"
      vars:
        pcp_package_state: "latest"
```
