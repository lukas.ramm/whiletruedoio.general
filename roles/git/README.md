# whiletruedoio.general.git

An Ansible Role to install and configure git.

## Description

Git is used in many development scenarios, but also for production systems that
facilitate GitOps-alike deployments. Therefore, having Git installed can be
useful.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.git"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.git"
      vars:
        git_package_state: "latest"
```
