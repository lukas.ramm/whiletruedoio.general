# whiletruedoio.general.epel

An Ansible Role to install the EPEL repositories.

## Description

The EPEL repositories provide lots of additional tools for RHEL, Rocky Linux
or AlmaLinux OS. This role ensures that the repository is configured properly.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.epel"
```
