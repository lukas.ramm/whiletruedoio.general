# whiletruedoio.general.dnf

An Ansible Role to install and configure dnf.

## Description

The DNF standard configuration might not be suitable for some cases. The role
ensures a slightly optimized configuration for faster download speeds.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.dnf"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.dnf"
      vars:
        dnf_package_state: "latest"
```
