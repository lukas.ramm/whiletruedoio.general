# whiletruedoio.general.thermald

An Ansible Role to install and configure thermald.

## Description

Thermald is a tool to adjust Intel CPUs properly and squeeze out a bit more
power savings or performance. This role installs it.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.thermald"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.thermald"
      vars:
        thermald_package_state: "latest"
```
