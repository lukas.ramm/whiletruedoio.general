# whiletruedoio.general.k3s

An Ansible Role to install and configure k3s.

## Description

k3s is a Kubernetes distribution with a small footprint, optimized for IoT and
Edge use cases. This role takes care of the setup, either for a single machine
or the entire cluster.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

Since we need to identify which host shall become a kubernetes server and which
will be an agent, you need to maintain an inventory file. The group names are
important, but can be overridden.

Single host inventory:

```yaml
all:
  children:
    k3s_servers:
      hosts:
        server01:
```

Single server with many agents:

```yaml
all:
  children:
    k3s_servers:
      hosts:
        server01:

    k3s_agents:
      hosts:
        agent01:
        agent02:
        agent03:
```

HA control plane with many agents:

```yaml
all:
  children:
    k3s_servers:
      hosts:
        server01:
        server02:
        server03:

    k3s_agents:
      hosts:
        agent01:
        agent02:
        agent03:
```

In a playbook, you can use the role like so.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.k3s"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.k3s"
      vars:
        k3s_registration_address: "192.168.90.200"
```
